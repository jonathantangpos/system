<?php
include "../bootstrap/config.php";
include "../bootstrap/_include.php";
$data = $_POST;
$username = $data['username'];
$user_id = $data['user_id'];
$firstname = $data['firstname'];
$lastname = $data['lastname'];

$status = $data['status'];
$password = $data['password'];


$user = new dbQuery();
$user->updateUser($username, $user_id, $firstname, $lastname,  $status);

if($password){
	$user->updatePassword($user_id, $password);
}

header ("location:".$url['users']);
?>