<?php
include "../bootstrap/config.php";
include "../bootstrap/_include.php";

$data = $_POST;

$reservation_id  = $data['reservation_id'];
$numdays  = $data['numdays'];
$total_amount  = $data['total_amount'];
$amount_received  = $data['amount_received'];
$room_id  = $data['room_id'];

$register = new dbQuery();
$register->savePayment($reservation_id, $numdays, $total_amount, $amount_received, $room_id);

header("location:".$url['reservation']."&pay=".$reservation_id);
?>