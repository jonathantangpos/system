<?php

/**
* $this = self
* $$hajaka$$
**/
class database {

    function connecDb() {

        $database = "system";
        $connect = mysqli_connect("localhost", "root", "",$database) or die("cannot connect to mysqli");
        

        return $connect;
    }

    function noreturnquery($query) {

        mysqli_query($this->connecDb(),$query);
    }

    function returnboolean($query) {

        $found = TRUE;
        $result = mysqli_query($this->connecDb(),$query);

        // $count = mysqli_num_rows($result);
        if (!$result || mysqli_num_rows($result) == 0)
            $found = FALSE;
        else
            $found = TRUE;

        
        return (boolean)$found;
    }

    function returnarray($query){
        
        $myarray =  Array();
        
        $result = mysqli_query($this->connecDb(),$query);
        
        while($rows = mysqli_fetch_assoc($result)){
            array_push($myarray, $rows);
        }
        
        return $myarray;
    }

}

?>
