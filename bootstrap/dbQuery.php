<?php

include 'Queries.php';

class dbQuery extends database {
	function registerUser($username,$firstname,$lastname,$password){
		$register = "insert into users (username, firstname, lastname, password, user_type, is_active) values('" . $username . "','" . $firstname . "','" . $lastname . "','" . $password . "','2', '0')";
		$this->noreturnquery($register);
	}

	function loginUser($username,$password){
		$login = "select * from users where username='" . $username . "' and password='" . $password . "' and is_active=1";
        return (boolean) $this->returnboolean($login);

	}

	function getUserbyId($id){
		$getInfo = "select * from users where id=".$id;
		return $this->returnarray($getInfo);
	}

	function updateUser($username, $user_id, $firstname, $lastname,  $status){
		$query = "update users set username='".$username."',firstname='".$firstname."',lastname='".$lastname."',is_active='".$status."' where id=".$user_id;
		$this->noreturnquery($query);
	}

	function updatePassword($id,$password){

		$query = "update users set password='".md5($password)."' where id=".$id;
		
		$this->noreturnquery($query);
	}

	function getUsers(){

		$getInfo = "select * from users ";
		return $this->returnarray($getInfo);
	}

	function getUserInfo($username, $password){

		$getInfo = "select * from users where username='" . $username . "' and password='" . $password . "'";
		return $this->returnarray($getInfo);
	}

	function getReservations(){
        $query = "select a.*,c.id as payment_id,c.total_amount,c.amount_received,c.num_days,b.rate,b.name as room_name from reservation a left join rooms b on  a.room_id=b.id left join payments c on a.id=c.reservation_id order by a.id desc";     
      
       	return $this->returnarray($query);
	}

	function getReservationbyId($id){
        $query = "select a.*,c.id as payment_id,c.total_amount,c.amount_received,c.num_days,b.rate,b.name as room_name  from reservation a left join rooms b on a.room_id=b.id left join payments c on a.id=c.reservation_id where a.id=".$id;    
        
       	return $this->returnarray($query);
	}

	function getReservationbyDate($date){
        $query = "select a.*,c.id as payment_id,c.total_amount,c.amount_received,c.num_days,b.rate,b.name as room_name  from reservation a left join rooms b on a.room_id=b.id left join payments c on a.id=c.reservation_id where a.date_created like '".$date."%'";    
        
       	return $this->returnarray($query);
	}

	function getRooms(){
        $query = "select * from rooms";    

       	return $this->returnarray($query);
	}

	function getRoombyId($id){
        $query = "select *  from rooms where id=".$id;    

       	return $this->returnarray($query);
	}

	function saveReservation($name, $address, $contact_number,$purpose,$room, $cidate, $codate, $userid){

		$query = "insert into reservation (user_id,name, address,contact_number, purpose, room_id, checkin, checkout) values('".$userid."','". $name ."','". $address ."','". $contact_number."','". $purpose . "','" . $room ."','". $cidate."','".$codate."')";

	    $this->noreturnquery($query);	
	    $this->updateRoomStatus($room, 'occupied');
	}

	function newRoom($name, $price){
		$query = "insert into rooms (name, rate,status) values('".$name."','".$price."','available')";
		$this->noreturnquery($query);	
	}

	function updateRoomStatus($room_id, $status){
		$query = "update rooms set status='".$status."' where id=".$room_id;
		$this->noreturnquery($query);	
	}

	function updateRoom($room_id, $name, $price){
		$query = "update rooms set name='".$name."',rate='".$price."' where id=".$room_id;
		$this->noreturnquery($query);	
	}

	function updateReservation($name, $address, $contact_number,$purpose,$room, $cidate, $codate, $id) {

        $query = "UPDATE reservation set checkout='" . $codate . "',name='".$name."', address='".$address."',contact_number='".$contact_number."',purpose='".$purpose."', room_id='".$room."',checkin='".$cidate."'   where id='".$id."'";
      
        $this->noreturnquery($query);
    }

    function checkPayment($id){
    	$query = "select * from payments where reservation_id=".$id;
    	
    	return $this->returnboolean($query);
    }

    function getPaymentData($id){
    	$query = "select * from payments where reservation_id=".$id;   

    	return $this->returnarray($query);
    }
    function savePayment($reservation_id, $numdays, $total_amount, $amount_received, $room_id){
    	$query = "insert into payments (reservation_id, total_amount, amount_received, num_days) values('".$reservation_id."','".$total_amount."','".$amount_received."','".$numdays."')";

    	$this->noreturnquery($query);
    	$this->updateRoomStatus($room_id, 'available');
    }



}

