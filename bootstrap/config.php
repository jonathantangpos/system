<?php
date_default_timezone_set('Asia/Singapore');

$env = 'local';
$_url = "";
if($env =='local'){
	$_url = 'http://localhost/system';
}

$url = array(
	'base_url' => $_url,
	'login' => $_url."/controller/loginUser.php",
	'register' => $_url."/controller/registerUser.php",
	'logout' => $_url."/controller/logoutuser.php",
	'home' => $_url.'/index.php?page=home',
	'rooms' => $_url.'/index.php?page=rooms',
	'users' => $_url.'/index.php?page=users',
	'reservation' => $_url.'/index.php?page=reservations',
	'login_err' => $_url.'/index.php?status=loginerr',
	'save_reservation' => $_url.'/controller/saveReservation.php',
	'update_reservation' => $_url.'/controller/updateReservation.php',
	'update_room' => $_url.'/controller/updateRoom.php',
	'save_room' => $_url.'/controller/saveRoom.php',
	'save_payment' => $_url.'/controller/savePayment.php',
	'update_user' => $_url.'/controller/updateUser.php',
	);


?>