<?php
include '/bootstrap/config.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main Page</title>
	<link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css"/>
	<link type="text/css" rel="stylesheet" href="assets/css/bootstrap-theme.min.css"/>
	<link type="text/css" rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css"/>
	<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>
</head>
<body>

<?php

$page = isset($_GET['page']) ? $_GET['page'] :'';

session_start();

include 'bootstrap/lookout.php';

if(!$logged){
	//not loggin
	include 'views/components/indicators.php';
	if($page == 'login'){
		include "views/login.php";
	}
	elseif($page == 'register'){
		include "views/register.php";
	}
	else{
		include "views/login.php";
	}

}else{

	if($page == 'receipt'){
		include 'views/print_receipt.php';
	}elseif($page == 'reports'){
		include 'views/report.php';
	}else{
		include 'views/layout.php';
	}
	
	
}

?>

<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>