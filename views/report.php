<style type="text/css">
	@media print{
		.print-button{
			display: none;
		}
	}
</style>
<?php
include "bootstrap/config.php";
include "bootstrap/_include.php";
$year = isset($_GET['year']) ? $_GET['year'] : '';
$month = isset($_GET['month']) ? $_GET['month'] : '';
$reservation = new dbQuery();
$reservations = $reservation->getReservationbyDate($year.'-'.$month);

?>
<br>
<br>
<br>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<form class="form-inline" method="GET">
				<input type="hidden" name="page" value="<?php echo $page; ?>">
			  <div class="form-group">
			    <label for="exampleInputName2">Month</label>
			    <select name="month" class="form-control">
			    	<option <?php echo ($month=='01') ? 'selected' : ''; ?> value="01">January</option>
			    	<option <?php echo ($month=='02') ? 'selected' : ''; ?> value="02">February</option>
			    	<option <?php echo ($month=='03') ? 'selected' : ''; ?> value="03">March</option>
			    	<option <?php echo ($month=='04') ? 'selected' : ''; ?> value="04">April</option>
			    	<option <?php echo ($month=='05') ? 'selected' : ''; ?> value="05">May</option>
			    	<option <?php echo ($month=='06') ? 'selected' : ''; ?> value="06">June</option>
			    	<option <?php echo ($month=='07') ? 'selected' : ''; ?> value="07">July</option>
			    	<option <?php echo ($month=='08') ? 'selected' : ''; ?> value="08">August</option>
			    	<option <?php echo ($month=='09') ? 'selected' : ''; ?> value="09">September</option>
			    	<option <?php echo ($month=='10') ? 'selected' : ''; ?> value="10">October</option>
			    	<option <?php echo ($month=='11') ? 'selected' : ''; ?> value="11">November</option>
			    	<option <?php echo ($month=='12') ? 'selected' : ''; ?> value="12">December</option>
			    </select>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail2">Year</label>
			    <select name="year" class="form-control">
			    	<option <?php echo ($year=='2017') ? 'selected' : ''; ?> value="2017">2017</option>
			    	<option <?php echo ($year=='2018') ? 'selected' : ''; ?> value="2018">2018</option>
			    	<option <?php echo ($year=='2019') ? 'selected' : ''; ?> value="2019">2019</option>
			    </select>
			  </div>
			  <button type="submit" class="btn btn-success">Generate Report</button>
			</form>
		</div>
		<br>
		<br>
		<br>
		<br>
	</div>
	<div class="row">
		<div class="col-md-12">			
			<table class="table table-striped">
				<thead>
					<th>Name</th>
					<th>Address</th>
					<th>Contact Number</th>
					<th>Purpose</th>
					<th>Room #</th>
					<th>Checkin Date</th>
					<th>Checkout Date</th>
				</thead>
				<tbody>
					<?php 
						if($reservations){
							foreach ($reservations as $value){
						
					?>
						<tr>
							<td><?php echo $value['name'];?></td>
							<td><?php echo $value['address'];?></td>
							<td><?php echo $value['contact_number'];?></td>
							<td><?php echo $value['purpose'];?></td>
							<td><?php echo $value['room_name'];?></td>
							<td><?php echo date_format(new DateTime($value['checkin']), 'F d, Y');?></td>
							<td><?php echo date_format(new DateTime($value['checkout']), 'F d, Y');?></td>
						</tr>
					<?php 

						}
					}else{
						?>
						<tr>
							<td colspan="7" style="text-align: center;">No Data Found</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>	
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<button type="submit" class="btn btn-info col-md-12 print-button" onclick="javascript: window.print();">Print</button>
		</div>
	</div>
</div>