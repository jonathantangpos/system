<?php
  include "bootstrap/_include.php";

    $reservation = new dbQuery();
    $rooms = $reservation->getRooms();
    $myarray = $reservation->getReservations();
?>
<div class="row">
    <div class="col-md-6">
        
        
        
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="panel-title">  
              Rooms Overview         
            </div>
        </div>
            <table class="table table-striped">
            <thead>
              <tr><th>Room Name</th><th>Price</th><th>Status</th></tr></thead>
            <tbody>
              <?php foreach($rooms as $room): ?>
                <tr>
                  <td><a  href="?page=home&id=<?=$room['id'] ?>"><?php echo $room['name']; ?></a></td>
                  <td><?php echo $room['rate']; ?></td>
                  <td><?php echo $room['status']; ?></td>
                </tr>
              <?php endforeach;?>
            </tbody>
            </table>
        </div><!--/panel-->
        
        
       
      </div>
    <div class="col-md-6">
  
        
       <div class="panel panel-default">
    <div class="panel-heading">Reservations Overview</div>
    <table class="table table-striped">
        <thead> 
            <tr> 
               
                <th>Name</th> 
                <th>Room Name</th> 
                <th>Checkout Date</th> 
                <th>Status</th> 
            </tr> 
        </thead>
    <?php
        foreach ($myarray as $value) {
          
          $checkout = $value['checkout'];
          $today = date("Y-m-d H:i:s");  
          $today = new DateTime($today); 
          $expiry_date = new DateTime($checkout);
          $interval = $today->diff($expiry_date);
          $days = (int) $interval->format('%R%a');
          $time = (int) $interval->format('%R%h');
          $min = (int) $interval->format('%R%i');
          
          if($days < 1 && $time < 1 && $min < 0){      
          }else{
          ?>
            <tr>
                
                <td><a href="?page=reservations&id=<?=$value['id'] ?>"><?php echo $value['name'];?></a></td>
                <td><?php echo $value['room_name']; ?></td>
                <td>
                <?php
                  echo date_format($expiry_date,"M d, y  g:i a");
                ?>
                  
                </td>
                <td>Not Expired</td>
            </tr>
          
          <?php
        }
        }
       
    ?>
    </table>
</div><!--/panel-->
        
          
        
          
        
  </div><!--/col-span-6-->

</div><!--/row-->