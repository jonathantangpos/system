<?php

include "bootstrap/config.php";

$_user = new dbQuery();
  
$user_id = isset($_GET['id']) ? $_GET['id'] : ''; 

$result = array();
$_url = "";
if($user_id):
  $result = $_user->getUserbyId($user_id);
  $_url = $url['update_user'];

?>
<div class="panel panel-default">
    <div class="panel-heading">
          <div class="panel-title">
                  <?php echo $result ? "Update" : "Create"; ?> User
               
          </div>
    </div>
    <div class="panel-body">

        <form class="form form-vertical" method="POST" action="<?php echo $_url; ?>">
         <?php if($result):?>
          <input type="hidden" name="user_id" value="<?php echo isset($result[0]['id']) ? $result[0]['id'] : ''; ?>">
         <?php endif;?>

          <div class="control-group">
                <label for="username" >Username</label>
                <div class="controls">
                    <input type="text" value="<?php echo $result[0]['username'];?>" class="form-control" name="username" placeholder="Username">
                </div>
            </div>
                
            <div class="control-group">
                <label for="firstname" >First Name</label>
                <div class="controls">
                    <input type="text" value="<?php echo $result[0]['firstname'];?>" class="form-control" name="firstname" placeholder="First Name">
                </div>
            </div>
            <div class="control-group">
                <label for="lastname" >Last Name</label>
                <div class="controls">
                    <input type="text" value="<?php echo $result[0]['lastname'];?>" class="form-control" name="lastname" placeholder="Last Name">
                </div>
            </div>
            
            
            <?php if($result[0]['user_type'] == 1): ?>
                <div class="control-group">
                    <label for="lastname" >User Type</label>
                    <div class="controls">
                        <select class="form-control" disabled>
                          <option <?php echo ($result[0]['user_type'] == '1') ? "selected": "";?> value="1">admin</option>
                          <option <?php echo ($result[0]['user_type'] == '2') ? "selected": "";?> value="2">member</option>
                        </select>
                    </div>
                </div>
              <div class="control-group">
                  <label for="lastname" >Status</label>
                  <div class="controls">
                      <select class="form-control" disabled>
                        <option <?php echo ($result[0]['is_active'] == '1') ? "selected": "";?> value="1">activate</option>
                        <option <?php echo ($result[0]['is_active'] == '0') ? "selected": "";?> value="0">deactivate</option>
                      </select>
                  </div>
              </div>
              <div class="control-group">
                  <label for="password" >Password</label>
                  <div class="controls">
                      <input type="password" class="form-control" disabled value="423423423423" placeholder="Password">
                  </div>
              </div>
            <?php else: ?>
                <div class="control-group">
                  <label for="lastname" >User Type</label>
                  <div class="controls">
                      <select class="form-control" disabled>
                        <option <?php echo ($result[0]['user_type'] == '1') ? "selected": "";?> value="1">admin</option>
                        <option <?php echo ($result[0]['user_type'] == '2') ? "selected": "";?> value="2">member</option>
                      </select>
                  </div>
              </div>
                <div class="control-group">
                    <label for="lastname" >Status</label>
                    <div class="controls">
                        <select class="form-control" name="status">
                          <option <?php echo ($result[0]['is_active'] == '1') ? "selected": "";?> value="1">activate</option>
                          <option <?php echo ($result[0]['is_active'] == '0') ? "selected": "";?> value="0">deactivate</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label for="password" >Password </label>
                    <div class="controls">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                        <small>(Leave empty if you do now want to update password)</small>
                    </div>
                </div>
            <?php endif; ?>
            
               
               
        
         <?php if($result[0]['user_type'] == 2): ?>
            <div class="control-group">
                <label></label>
              <div class="controls">
              <button type="submit" class="btn btn-success">
                Update
            </button>
              </div>
            </div>
             <?php endif;?>
          
        </form>
  
  
    </div><!--/panel content-->
  </div><!--/panel-->
  <?php endif; ?>