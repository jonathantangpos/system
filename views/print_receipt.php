<?php

include "bootstrap/config.php";
include "bootstrap/_include.php";

$reservation = new dbQuery();
	
$reservaton_id = isset($_GET['id']) ? $_GET['id'] : ''; 

$myarray = array();

if($reservaton_id){
	$myarray = $reservation->getReservationbyId($reservaton_id);
	
}


$checkout = $myarray[0]['checkout'];
$today = date("Y-m-d H:i:s");  
$today = new DateTime($today); 
$expiry_date = new DateTime($checkout);
?>
<br>
<br>
<br>
<br>
<br>
<div class="container" >
	<div class="row">
	<div class="col-md-4 col-md-offset-4" style="border: 1px solid #ccc;">
		<h1>MINI HOTEL </h1>
		<br>
		<br>
		<br>
		<div class="row">
		     <div class="col-md-12">
		         <div class="row">
		             
		             <div class="col-xs-6 col-sm-6 col-md-6 text-right">
		                 <p>
		                     <em>Date: <?php echo date_format($today, 'F d, Y');?></em>
		                 </p>
		                 <p>
		                     <em>Receipt #: <?php echo strtotime($checkout)."-".$myarray [0]['id'];?></em>
		                 </p>
		             </div>
		         </div>
		         <div class="row">
		             <table class="table table-hover">
		                 <thead>
		                     <tr>
		                         <th>Name</th>
		                         <th>Number of Days</th>
		                         <th class="text-center">Rate</th>
		                         <th class="text-center">Total</th>
		                     </tr>
		                 </thead>
		                 <tbody>
		                     <tr>
		                         <td class="col-md-9"><em><?php echo $myarray [0]['name']; ?></em></h4></td>
		                         <td class="col-md-1" style="text-align: center"> <?php echo $myarray [0]['num_days']; ?> </td>
		                         <td class="col-md-1 text-center">PHP<?php echo $myarray [0]['rate']; ?></td>
		                         <td class="col-md-1 text-center">PHP<?php echo $myarray [0]['total_amount']; ?></td>
		                     </tr>

		                     
		                    
		                     <tr>
		                         <td>   </td>
		                         <td>   </td>
		                         <td class="text-right">
		                         <h6><strong>Total: </strong></h6>
		                         <h6><strong>Received: </strong></h6>
		                         <h6><strong>Change: </strong></h6>
		                         </td>
		                         <td class="text-center text-danger">
		                             <h6><strong>PHP<?php echo $myarray [0]['total_amount']; ?></strong></h6>
		                             <h6>PHP<?php echo $myarray [0]['amount_received']; ?></h6>
		                             <h6>PHP<?php echo (int)$myarray [0]['amount_received'] - (int)$myarray [0]['total_amount']; ?></h6>
		                         </td>
		                     </tr>
		                     
		                 </tbody>
		             </table>
		             
		             
		         </div>
		     </div>
		
		</div>
		
	</div>
	</div>
</div>
<script type="text/javascript">
	window.print();
</script>