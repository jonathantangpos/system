<!-- Header -->
<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
  <div class="navbar-header">
  	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    	<span class="icon-toggle"></span>
  	</button>
  	<a class="navbar-brand" >Mini Hotel</a>
  </div>
  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav pull-right">
      
      
      
      <li><a href="<?php echo $url['logout']; ?>">Log out</a></li>
    </ul>
  </div>
</div>
<!-- /Header -->

<!-- Main -->
<div class="container">
<div class="row">
	<div class="col-md-3">
      <!-- Left -->
      
      <hr>
      <ul class="nav nav-pills nav-stacked">
        <li class="nav-header"></li>
        <li class="<?php echo ($page=='home') ? 'active' : '';?>"><a href="?page=home"><i class="glyphicon  glyphicon-home"></i> Home</a></li>
        <li class="<?php echo ($page=='reservations') ? 'active' : '';?>"><a href="?page=reservations"><i class="glyphicon  glyphicon-book"></i> Reservations</a></li> 
        <?php if($_SESSION['usertype'] == '1'):?>      
        <li class="<?php echo ($page=='users') ? 'active' : '';?>"><a href="?page=users"><i class="glyphicon  glyphicon-user"></i> Users</a></li>
        <?php endif;?>      
        <li class="<?php echo ($page=='rooms') ? 'active' : '';?>"><a href="?page=rooms" title="Bootstrap 3 Icons"><i class="glyphicon  glyphicon-list-alt"></i> Rooms</a></li>
      </ul>
  	</div><!-- /span-3 -->
    <div class="col-md-9">
      	<!-- Right -->
      	
       
      <?php
      	switch ($page) {
		case 'home':
			include 'views/home.php';
			break;
		case 'reservations':
			include 'views/reservations.php';
			break;
		case 'users':
			include 'views/users.php';
			break;
    case 'rooms':
      include 'views/rooms.php';
      break;
		default:
			include 'views/home.php';
			break;
	}
      ?>


      
  	</div><!--/col-span-9-->
</div>
</div>
<!-- /Main -->



  