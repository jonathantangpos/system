<?php

include "bootstrap/config.php";

$reservation = new dbQuery();
$rooms = $reservation->getRooms();
	
$reservaton_id = isset($_GET['id']) ? $_GET['id'] : ''; 

$result = array();
$_url = $url['save_reservation'];
if($reservaton_id){
	$result = $reservation->getReservationbyId($reservaton_id);
	$_url = $url['update_reservation'];
}

?>

<div class="panel panel-default">
	<div class="panel-heading">
	      <div class="panel-title">
	      <?php echo $result ? "Update" : "Create"; ?> Reservation
	      </div>
	</div>
	<div class="panel-body">

	    <form class="form form-vertical" method="POST" action="<?php echo $_url;?>">
	     <?php if($result):?>
	     	<input type="hidden" name="reservation_id" value="<?php echo isset($result[0]['id']) ? $result[0]['id'] : ''; ?>">
	     	<input type="hidden" name="room" value="<?php echo isset($result[0]['room_id']) ? $result[0]['room_id'] : ''; ?>">
	     <?php endif;?>
	      <div class="control-group">
	        <label>Name</label>
	        <div class="controls">
	         <input type="text" name="name"  class="form-control" value="<?php echo isset($result[0]['name']) ? $result[0]['name'] : ''; ?>" placeholder="Name">
	        </div>
	      </div>      
	      
	      <div class="control-group">
	        <label>Address</label>
	        <div class="controls">
	         <input type="text" class="form-control" name= "address" value="<?php echo isset($result[0]['address']) ? $result[0]['address'] : ''; ?>" placeholder="Address">
	        
	        </div>
	      </div> 
	       <div class="control-group">
	        <label>Contact number</label>
	        <div class="controls">
	         <input type="text" class="form-control" name= "contact" value="<?php echo isset($result[0]['contact_number']) ? $result[0]['contact_number'] : ''; ?>" placeholder="Contact number">
	        
	        </div>
	      </div>     
	      
	      <div class="control-group">
	        <label>Purpose</label>
	        <div class="controls">
	          <textarea class="form-control" name="purpose"><?php echo isset($result[0]['purpose']) ? $result[0]['purpose'] : ''; ?></textarea>
	        </div>
	      </div> 
	      <div class="control-group">
	        <label>Room </label>
	        <div class="controls">
	        	<?php if($result):?>
	           <select disabled class="form-control" >
		           <?php 
		           	foreach($rooms as $room): 		           		
		           ?>

		           		<option selected="<?php echo ($result[0][id] == $room['id']) ? 'selected' : ''; ?>" value="<?php echo $room['id']; ?>"><?php echo $room['name'];?> (PHP <?php echo $room['rate'];?>)</option>
		           <?php 
		           		
		           	endforeach; 
		           ?>
	           </select>
	       <?php else:?>
	       	<select class="form-control" name='room'>
	       		<?php 
	       			foreach($rooms as $room): 
	       				if($room['status'] == 'available'):
			       		?>
			       			<option value="<?php echo $room['id']; ?>"><?php echo $room['name'];?> (PHP <?php echo $room['rate'];?>)</option>
			       		<?php 
			       		endif;
	       			endforeach; 
	       		?>
	       		</select>
	       <?php endif;?>
	        </div>
	      </div>      
	        
	      <div class="control-group">
	        <label>Checkin Date</label>
	        <div class="controls">
	        <input type="text" class="form-control datetimepicker" value="<?php echo isset($result[0]['checkin']) ? $result[0]['checkin'] : ''; ?>" name="cidate">
	        </div>
	      </div> 
	      <div class="control-group">
	        <label>Checkout Date</label>
	        <div class="controls">
	        	<input type="text" class="form-control datetimepicker" value="<?php echo isset($result[0]['checkout']) ? $result[0]['checkout'] : ''; ?>" name="codate" >
	        </div>
	      </div>  
	      
	      <div class="control-group">
	          <label></label>
	        <div class="controls">

	        <button type="submit" class="btn btn-success">
	            <?php echo $result ? "Update" : "Create"; ?>
	        </button>


	        </div>
	      </div>   
	      
	    </form>


	</div><!--/panel content-->
	</div><!--/panel-->