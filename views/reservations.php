<div class="row">
	<div class="col-md-12">		
		<a title="Add Widget" data-toggle="modal" href="?page=reservations" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus-sign"></span> New Reservation</a>
		<br>
		<br>
	</div>
</div>         
<div class="row">
    <div class="col-md-6">
        
            <?php
            	include 'listReservation.php';
            ?>
        
      </div>
    <div class="col-md-6">
  
        <?php
            $pay = isset($_GET['pay']) ? $_GET['pay'] : ''; 
            if($pay){
                include 'payform.php';
            }else{
        	   include 'addReservation.php';
            }
        ?>
          
        
	</div><!--/col-span-6-->

</div><!--/row-->
