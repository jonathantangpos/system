<div class="container">  
 	<div id="signupbox" style=" margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
		<div class="panel panel-info">
		    <div class="panel-heading">
		        <div class="panel-title">Sign Up</div>
		        <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="?page=login">Sign In</a></div>
		    </div>  
	    	<div class="panel-body" >
	        	<form id="signupform" method="POST" action="<?php echo $url['register'];?>" class="form-horizontal" role="form">		              
		            <div class="form-group">
		                <label for="username" class="col-md-3 control-label">Username</label>
		                <div class="col-md-9">
		                    <input type="text" class="form-control" name="username" placeholder="Username">
		                </div>
		            </div>
		                
		            <div class="form-group">
		                <label for="firstname" class="col-md-3 control-label">First Name</label>
		                <div class="col-md-9">
		                    <input type="text" class="form-control" name="firstname" placeholder="First Name">
		                </div>
		            </div>
		            <div class="form-group">
		                <label for="lastname" class="col-md-3 control-label">Last Name</label>
		                <div class="col-md-9">
		                    <input type="text" class="form-control" name="lastname" placeholder="Last Name">
		                </div>
		            </div>
		            <div class="form-group">
		                <label for="password" class="col-md-3 control-label">Password</label>
		                <div class="col-md-9">
		                    <input type="password" class="form-control" name="password" placeholder="Password">
		                </div>
		            </div>

		            <div class="form-group">
		                <!-- Button -->                                        
		                <div class="col-md-offset-3 col-md-9">
		                    <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i> Sign Up</button>
		                </div>
		            </div>	            
	        	</form>
	     	</div>
		</div>
	</div> 
</div>