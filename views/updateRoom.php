<?php

include "bootstrap/config.php";

$room = new dbQuery();
  
$room_id = isset($_GET['id']) ? $_GET['id'] : ''; 

$result = array();
$_url = $url['save_room'];
if($room_id){
  $result = $room->getRoombyId($room_id);
  $_url = $url['update_room'];
}

?>
<div class="panel panel-default">
    <div class="panel-heading">
          <div class="panel-title">
                  <?php echo $result ? "Update" : "Create"; ?> Room
               
          </div>
    </div>
    <div class="panel-body">

        <form class="form form-vertical" method="POST" action="<?php echo $_url; ?>">
         <?php if($result):?>
          <input type="hidden" name="room_id" value="<?php echo isset($result[0]['id']) ? $result[0]['id'] : ''; ?>">
         <?php endif;?>

          <div class="control-group">
            <label>Room Name</label>
            <div class="controls">
             <input type="text" class="form-control" name="room_name" value="<?php echo isset($result[0]['name']) ? $result[0]['name'] : ''; ?>">
            </div>
          </div>      
          
          <div class="control-group">
            <label>Price</label>
            <div class="controls">
             <input type="text" class="form-control" name="rate" value="<?php echo isset($result[0]['rate']) ? $result[0]['rate'] : ''; ?>">
            
            </div>
          </div>   
          
          <div class="control-group">
            <label>Status</label>
            <div class="controls">
              <input type="text" class="form-control" readonly value="<?php echo isset($result[0]['status']) ? $result[0]['status'] : ''; ?>">
            </div>
          </div> 
               
         
          <div class="control-group">
              <label></label>
            <div class="controls">
            <button type="submit" class="btn btn-success">
              <?php echo $result ? "Update" : "Create"; ?>
          </button>
            </div>
          </div>   
          
        </form>
  
  
    </div><!--/panel content-->
  </div><!--/panel-->