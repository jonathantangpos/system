<?php
  include "bootstrap/_include.php";

    $reservation = new dbQuery();
    $rooms = $reservation->getRooms();
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">  
      Rooms Overview         
    </div>
</div>
    <table class="table table-striped">
    <thead>
      <tr><th>Room Name</th><th>Price</th><th>Status</th></tr></thead>
    <tbody>
      <?php foreach($rooms as $room): ?>
        <tr>
          <td><a  href="?page=rooms&id=<?=$room['id'] ?>"><?php echo $room['name']; ?></a></td>
          <td><?php echo $room['rate']; ?></td>
          <td><?php echo $room['status']; ?></td>
        </tr>
      <?php endforeach;?>
    </tbody>
    </table>
</div><!--/panel-->