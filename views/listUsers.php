<?php
  include "bootstrap/_include.php";

    $_user = new dbQuery();
    $users = $_user->getUsers();
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">  
      Users          
    </div>
</div>
    <table class="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Username</th>
        <th>Type</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($users as $user): ?>
        <tr>
          <td><a  href="?page=users&id=<?=$user['id'] ?>"><?php echo $user['firstname']." ".$user['lastname']; ?></a></td>
          <td><?php echo $user['username']; ?></td>
          <td><?php echo ($user['user_type'] == '1') ? "admin" : 'member'; ?></td>
          <td><?php echo ($user['is_active'] == '0') ? "inactive" : "active"; ?></td>
        </tr>
      <?php endforeach;?>
    </tbody>
    </table>
</div><!--/panel-->