<?php
	include "bootstrap/_include.php";

    $reservation = new dbQuery();
    $myarray = $reservation->getReservations();

?>
<div class="panel panel-default">
    <div class="panel-heading">
    Reservations
    <a target="_blank" class="pull-right" href="?page=reports">Reports</a></div>
    <table class="table table-striped">
        <thead> 
            <tr> 
                <th>Name</th> 
                <th>Room Name</th> 
                <th>Reservation <br>Status</th> 
                <th>Payment <br>Status</th> 
                <th></th> 
            </tr> 
        </thead>
    <?php
        foreach ($myarray as $value) {
        	?>
            <tr>
                <td><a data-toggle="modal" data-target="#myModal<?=$value['id'] ?>"><?php echo $value['name'];?></a></td>
                <td><?php echo $value['room_name']; ?></td>
                <td>
                <?php
                    $checkout = $value['checkout'];
                    $today = date("Y-m-d H:i:s");  
                    $today = new DateTime($today); 
                    $expiry_date = new DateTime($checkout);
                    $interval = $today->diff($expiry_date);
                    $days = (int) $interval->format('%R%a');
                    $time = (int) $interval->format('%R%h');
                    $min = (int) $interval->format('%R%i');
                    
                    $reservation_status = 'Not Expired';
                    if($days < 1 && $time < 1 && $min < 0){                        
                       $reservation_status = "Expired";
                    }

                    echo $reservation_status;
                    
                ?>
                </td>
                <td>
                <?php if($value['payment_id']):?>
                Paid
                <?php else:?>
                <a href="?page=reservations&pay=<?=$value['id'] ?>">Pay</a>
                <?php endif;?>
                </td>
                <td>
                    <a title="Edit" href="?page=reservations&id=<?=$value['id'] ?>"><i class="glyphicon glyphicon-pencil"></i></a>
                    <?php if($value['payment_id']):?>
                    <!-- Modal -->
                    <div id="myModal<?=$value['id'] ?>" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Transaction Receipt </h4>
                          </div>
                          <div class="modal-body">
                            
                            <?php include "components/receipt.php";?>


                          </div>
                          <div class="modal-footer">
                            <a href="?page=receipt&id=<?=$value['id'] ?>" target="_blank" class="btn btn-default" >Print Receipt</a>
                          </div>
                        </div>

                      </div>
                    </div>
                    <?php endif;?>
                </td>
            </tr>
        	
        	<?php

        }
       
    ?>
    </table>
</div><!--/panel-->