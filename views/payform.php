<?php

include "bootstrap/config.php";

$payment = new dbQuery();
  
$reservation_id = isset($_GET['pay']) ? $_GET['pay'] : ''; 

$result = array();
$_url = $url['save_payment'];
if($reservation_id){
  $exist = $payment->checkPayment($reservation_id);
  if($exist){
    $result = $payment->getPaymentData($reservation_id);
  }
}

$reservation = $payment->getReservationbyId($reservation_id);


$checkout = $reservation[0]['checkout'];
$checkin = $reservation[0]['checkin'];

$checkout = new DateTime($checkout);
$checkin = new DateTime($checkin);

$interval = $checkin->diff($checkout);
$days = (int) $interval->format('%R%a');



$days = ($days == 0) ? 1 : $days;

$amount = (int)$days * (int)$reservation[0]['rate'];


?>
<div class="panel panel-default">
    <div class="panel-heading">
          <div class="panel-title">
              Save Payment               
          </div>
    </div>
    <div class="panel-body">

        <form class="form form-vertical" method="POST" action="<?php echo $_url; ?>"> 
          <?php if(!$result):?>        
            <input type="hidden" name="reservation_id" value="<?php echo $reservation_id; ?>">
            <input type="hidden" name="numdays" value="<?php echo $days; ?>">
            <input type="hidden" name="total_amount" value="<?php echo $amount; ?>">
            <input type="hidden" name="room_id" value="<?php echo $reservation[0]['room_id']; ?>">
          <?php endif;?>
          <div class="control-group">
            <label>Number of Days</label>
            <div class="controls">
              <input type="text" readonly class="form-control" value="<?php echo isset($result[0]['num_days']) ? $result[0]['num_days'] : $days; ?>">
            </div>
          </div> 
          <div class="control-group">
            <label>Room Rate</label>
            <div class="controls">
              <input type="text" readonly class="form-control" value="<?php echo isset($reservation[0]['rate']) ? $reservation[0]['rate'] : ''; ?>">
            </div>
          </div> 
          <div class="control-group">
            <label>Total Amount</label>
            <div class="controls">
             <input type="text" readonly class="form-control" name="total_amount" value="<?php echo isset($result[0]['total_amount']) ? $result[0]['total_amount'] : $amount; ?>">
            </div>
          </div>      
          
          <div class="control-group">
            <label>Amount Received</label>
            <div class="controls">
             <input type="text" class="form-control" name="amount_received" value="<?php echo isset($result[0]['amount_received']) ? $result[0]['amount_received'] : ''; ?>">
            
            </div>
          </div>   
          
          
               
         
          <div class="control-group">
              <label></label>
            <div class="controls">
            <?php if($result):?>
              <button type="submit" disabled class="btn btn-success">  Paid </button>
            <?php else:?>
              <button type="submit"  class="btn btn-success">  Pay </button>
            <?php endif;?>
            </div>
          </div>   
          
        </form>
  
  
    </div><!--/panel content-->
  </div><!--/panel-->