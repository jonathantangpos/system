   <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                   
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <p>
                        <em>Date: <?php echo date_format($today, 'F d, Y');?></em>
                    </p>
                    <p>
                        <em>Receipt #: <?php echo strtotime($checkout)."-".$value['id'];?></em>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="text-center">
                   
                </div>
                </span>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Number of Days</th>
                            <th class="text-center">Rate</th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-9"><em><?php echo $value['name']; ?></em></h4></td>
                            <td class="col-md-1" style="text-align: center"> <?php echo $value['num_days']; ?> </td>
                            <td class="col-md-1 text-center">PHP<?php echo $value['rate']; ?></td>
                            <td class="col-md-1 text-center">PHP<?php echo $value['total_amount']; ?></td>
                        </tr>

                        
                       
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td class="text-right">
                            <h6><strong>Total: </strong></h6>
                            <h6><strong>Received: </strong></h6>
                            <h6><strong>Change: </strong></h6>
                            </td>
                            <td class="text-center text-danger">
                                <h6><strong>PHP<?php echo $value['total_amount']; ?></strong></h6>
                                <h6>PHP<?php echo $value['amount_received']; ?></h6>
                                <h6>PHP<?php echo (int)$value['amount_received'] - (int)$value['total_amount']; ?></h6>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
                <div>
                
                </div>
            </div>
        </div>
   